import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux'
import firebase from 'firebase';
import './App.css';

//Components
import Login from './components/navigation/Login/Login';
import Home from './components/navigation/Home/Home';
import BookPage from './components/navigation/BookPage/BookPage';
import Layout from './hoc/Layout/Layout';
import { checkUserSignedIn } from './actions/UserAction';
import { SEND_USER } from './actions/ActionTypes';




class App extends Component {
  componentWillMount () {
    // var config = {
    //   apiKey: "AIzaSyBxOTO0OU5UxwTrGKp04LrEYUqe1phXgVY",
    //   authDomain: "church-library-1e9dc.firebaseapp.com",
    //   databaseURL: "https://church-library-1e9dc.firebaseio.com",
    //   projectId: "church-library-1e9dc",
    //   storageBucket: "church-library-1e9dc.appspot.com",
    //   messagingSenderId: "45539860131"
    // };
    var config = {
      apiKey: "AIzaSyBQUzkPT9kFYM51HTSxdkNyCz7lYmr3PLA",
      authDomain: "livraria-ipsm.firebaseapp.com",
      databaseURL: "https://livraria-ipsm.firebaseio.com",
      projectId: "livraria-ipsm",
      storageBucket: "livraria-ipsm.appspot.com",
      messagingSenderId: "333995991372"
    };
    firebase.initializeApp(config);      
  }  

  render() {
    return (      
      <Layout>
         <Switch>
          <Route path='/login' render={({ history }) => (
             <Login 
              {...history}
             />)
            } 
          />                                                
          <Route path='/book/:bookId' render={({ history }) => 
            <BookPage 
              {...history}
            />
            }
          />                                          
          <Route path='/book/new' render={({ history }) => 
            <BookPage 
              {...history}
            />
            }
          />           
          <Route path='/' render={({ history}) => (
            <Home 
              {...history}
            />            
             )
            } 
          />                      
        </Switch>    
      </Layout>
    );
  }
}

export default App;
