import currency from 'currency.js';

export const BRL = value => 
  currency(value, { precision: 2, separator: '.', decimal: ',', symbol: 'R$ ', formatWithSymbol: true });
export const NUMBER = value => 
  currency(value, { precision: 0, separator: '.', symbol: '', formatWithSymbol: true });