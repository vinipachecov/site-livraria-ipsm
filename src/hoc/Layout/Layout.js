import React, { Component } from 'react';
import Aux from '../Aux/Aux';
import Header from '../../components/Header/Header';
import { connect } from 'react-redux'
import { checkUserSignedIn } from '../../actions/UserAction';

export class Layout extends Component {
  state = {
    showSideDrawer: false
  }

  componentDidMount = async () =>  {   
    await this.props.checkUserSignedIn();    
  }

  render () {
    const { user } = this.props;    
    return(
        <Aux>
          <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
          <Header 
            user={user} 
            onLogOut={checkUserSignedIn}  
          />                    
          <main className="mdl-layout__content">
            <div className="page-content" style={{ width: '100%'}}>
              {this.props.children}
            </div>
          </main>
          </div>
        </Aux>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.userData.user  
})

const mapDispatchToProps = {
  checkUserSignedIn    
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout)

