import React, { Component } from 'react';
import { connect } from 'react-redux';
import classes from './Login.css';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
 import firebase from 'firebase';
import { SEND_USER } from '../../../actions/ActionTypes';

export class Login extends Component {  
  state = {
    username:'',
    password:'',
    error: '',
    loading: false
  }

  componentDidMount() { 
    const { user, push } = this.props;    
    if (user) {
      push('/home')
    }         
  }

  onUserNameChangeText = (event) => {
    this.setState({ username: event.target.value });
  }

  onPasswordChangeText = (event) => {
    this.setState({ password: event.target.value });
  }

  onSignIn = async (event) => {   
    event.preventDefault();     
    const { username, password } = this.state;
    this.setState({ loading: true });
    try {
      const user = await firebase.auth().signInWithEmailAndPassword(username, password);      
      this.props.dispatch({ SEND_USER, payload: user });
      this.props.push('/home');      
      this.setState({ loading: false });
    } catch (error) {
      this.setState({ loading: false, error });
      console.log(error);      
    }    
  }  

  onUserSignedIn = (user) => {
    const { push } = this.props;
    if (user) {
      push('/');
    }        
  }

  onRenderMessage = () => {
    const { error } = this.state;
    if (error) {
      setTimeout(() => {
        this.setState({ error: '' });
      }, 5000)
        return (
          <div class="alert alert-danger" role="alert">
            Não foi possível fazer o login, verifique os seus dados e tente novamente.
          </div>
        )

    }
  }

  render() {    
    const { loading } = this.state;
    console.log('loading = ', loading);    
    const { user } = this.props;
    this.onUserSignedIn(user)
    return (      
       <div>         
         <div style={{ height: '100vh', alignItems: 'center'}}> 
         {
          this.onRenderMessage()
        }           
        <div>
            <img 
              src="https://firebasestorage.googleapis.com/v0/b/livraria-ipsm.appspot.com/o/LOGO%20E%20IGREJA.png?alt=media&token=58c80f16-fa8c-4796-98ea-c29a8c362f87"                  
              class={classes.image}
              alt={'Igreja Presbiteriana em Santa Maria'}
            />
            <form className={classes.form}>
              <div class={["form-group", classes.formGroup].join(' ')}>  
                <label>Email</label>                                            
                <input 
                  type="email" 
                  class="form-control" 
                  id="exampleInputEmail1" 
                  aria-describedby="emailHelp" 
                  placeholder="Email"
                  onChange={event => this.onUserNameChangeText(event)}   
                  value={this.state.username}                  
                />                
                </div>
                <div class={["form-group", classes.formGroup].join(' ')}>       
                <label>Senha</label>                                            
                <input 
                  type="password" 
                  class="form-control" 
                  id="exampleInputEmail1" 
                  aria-describedby="emailHelp" 
                  placeholder="Senha"                  
                  onChange={event => this.onPasswordChangeText(event)}   
                  value={this.state.password}    
                />                                       
                </div>            
                <button 
                  onClick={event => this.onSignIn(event)} 
                  type="submit" 
                  class={["btn", classes.button].join(' ')}>
                  Entrar
                </button>      
                {
                  loading ?                  
                  <CircularProgress 
                    className={classes.progress} 
                    style={{ 
                      color: 'rgb(17, 117, 25)',
                      marginTop: '20px'
                    }}                    
                  />
                  :
                  null
                }
            </form>               
            </div>
          </div>
      </div>
    )
  }
}


const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,    
  },
});

const mapStateToProps = (state) => ({
  user: state.userData.user
})


export default connect(mapStateToProps)(withStyles(styles)(Login))
