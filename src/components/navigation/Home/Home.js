import React, { Component } from 'react';
import Book from '../../Book/Book';
import { connect} from 'react-redux';

import classes from './Home.css';
import { getBooks, selectBook, getFilteredBooks } from '../../../actions/bookActions';
import { CircularProgress, withStyles } from '@material-ui/core';
import { List, AutoSizer } from "react-virtualized";


class Home extends Component {

  state = {    
    loading: false
  }

  async componentDidMount() {       
    this.setState({ loading: true });    
    await this.props.getBooks();      
    this.setState({ loading: false });
  }

  onCreateNewBook = () => {
    const { push } = this.props;
    push('/book/new');
  }
  

  onSearchTextChange = async (event) => {              
    const { value } = event.target;    
    const { bookList } = this.props;
    console.log('texto de busca = ', value);
    if (value) {      
      this.props.getFilteredBooks(bookList, value);
    } else {
      this.props.getFilteredBooks(bookList, '');      
    }        
  }

  renderRow = ({ index, key, style }) => {  
    const { filteredBooks, bookList } = this.props;        
    if (filteredBooks.length > 0) {      
      return (              
        <li 
        className="list-group-item" style={style} key={key} >
        <Book
          data={filteredBooks[index]}
          onPress={selectBook}                                                   
        />
      </li>        
      )              
    } else {
       if(bookList.length > 0) {
        return (            
          <li 
          className="list-group-item"          
        >
          <Book
            data={bookList[index]}
            onPress={selectBook}                                                   
          />
        </li>              
        )              
       } else {
          return <div>carregando livros...</div>
       }
    }
    
  } 

  

  render() {        
    const { user, filteredBooks, bookList, searchText } = this.props;
    // console.log('lista de livros filtrados = ', filteredBooks);
    const { loading } = this.state;                
    const rowHeight = 200;
    const rowWidth = 800;
    return (        
      <div>           
          <div className='row' style={{ display: 'flex', flexDirection: 'column'}}>          
              <input 
                className={['form-control', classes.searchBar].join(' ')}                
                value={searchText}                 
                type="text" 
                placeholder="Digite o nome ou editora de um livro!"
                onChange={event => this.onSearchTextChange(event)}            
              />            
        </div>
        <div >
          {
            !loading ?
            <ul className={'list-group'}>             
            {
              filteredBooks.map(book => {
              return( 
                <li 
                  className="list-group-item"          
                  key={book.id}
                >
                  <Book
                    data={book}
                    onPress={selectBook}                                                   
                  />
                </li>              
                )              
              })
            }
            {/* <List             
              width={rowWidth}
              height={800}
              rowHeight={rowHeight}
              rowRenderer={this.renderRow}
              rowCount={bookList.length}
              overscanRowCount={3} 
            /> */}
            </ul>
          :
          <div
            style={{
              display: 'flex',
              width: '100%',
              alignItems: 'center',              
            }}
          >
            <CircularProgress             
              style={{               
                color: 'rgb(17, 117, 25)',
                margin: '20px auto'
              }}                    
            />          
          </div>          
          }       
          {
            user ?
            <button 
            className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
            style={{ 
              bottom: 40,
              right: 40, 
              position: 'fixed', 
              backgroundColor: '#117519',
              zIndex: 2
            }}
            onClick={this.onCreateNewBook}
          >
            <i className="material-icons">add</i>
          </button> 
          :
          null
          }            
          </div>    
        </div>
    );
  }
}

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,    
  },
});

const mapStateToProps = (state) => ({
  bookList: state.bookData.bookList,
  user: state.userData.user,
  filteredBooks: state.bookData.filteredBooks
})

const mapDispatchToProps = {
  getBooks,
  selectBook,
  getFilteredBooks
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Home));
