import React from 'react'
import { red } from '../../../../utils/colors';
import classes from './BookDescription.css';


const BookDescription = (props) => {    
  const { book } = props;  
  return (    
      <div className="mdl-grid">
        <div style={{ display: 'flex', alignItems: 'flex-start' }}>
        {
          book.imageURL ?
          <img
            src={book.imageURL}
          />
          :
          null
        }          
        </div>
        <div           
          className={["mdl-cell mdl-cell--4-col", classes.description].join(' ')}
        >          
          <p style={{ textAlign: 'justify'}}><strong>Descrição</strong>: {book.description || 'Em breve.'}</p>
        </div>
        <div 
          className="mdl-cell mdl-cell--4-col">
          <p style={{ fontSize: '20px'}}>Título: {book.name}</p>                    
          <p>Por {book.author}</p>
          <p>Disponível:  {+book.quantity >= 1 ? 'Em estoque' : 'Indisponível'}</p>
            <p
              style={{ 
                color: red,
                fontSize: '20px'
              }}
            >
              Preço: R${book.price.replace('.', ',')}
            </p>
        </div>
      </div>
  )
}

export default BookDescription;