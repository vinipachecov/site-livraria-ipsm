import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';
import { getBookById, updateBoook, createBook } from '../../../actions/bookActions';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css'; 
import firebase from 'firebase';
import ReactDOM from 'react-dom';
import BookDescription from './BookDescription/BookDescription';
import { withStyles, CircularProgress } from '@material-ui/core';


class BookPage extends Component {  

  state = {
    isLogged: false,
    isNewBook: false,
    id: '',
    name: '',
    author: '',
    publisher: '',
    quantity: '',
    price: '',
    image: '',
    imageURL: '',
    croppedImage: null,
    newImage: false,
    description: '',
    loading: false,
    loadPage: false,
    completeTask: 'none'
  }

  componentWillUnmount() {
    this.setState({
      isNewBook: false,
      id: '',
      name: '',
      author: '',
      publisher: '',
      quantity: '',
      price: '',
      image: '',
      currentImage: '',
      imageURL: '',
      croppedImage: null,
      newImage: false,
      description: '',
      loading: false,
      completeTask: 'none'
    });
  }

  async componentDidMount() {   
    //Scroll to the top 
    ReactDOM.findDOMNode(this).scrollIntoView();   
    this.setState({ loadPage: true });
       
    const { pathname } = this.props.location;
    const { bookSelected, push } = this.props;
    let isNewBook = false;
    let imageref;

    firebase.auth().onAuthStateChanged(user => {
      if (!user) {                                    
        this.setState({ isLogged: false });
      } else {
        this.setState({ isLogged: true });
      }                         
    }); 
    const param = pathname.split('/')[2];  
    if (param === 'new') {        
      isNewBook = true;
      this.setState({ isNewBook });
      return;
    }
 

    // check if user is landing directly here or 
    // just refreshed
    if (bookSelected.name === undefined) {
      
      // fetch from firebase again 
      const book = await this.props.getBookById(param);                
      this.setState({
        id: book.id,
        name: book.name,
        author: book.author,
        quantity: book.quantity,
        publisher: book.publisher ? book.publisher : '',
        price: book.unitPrice ? book.unitPrice : '',
        imageURL: book.imageURL, 
        currentImage: book.imageURL,       
        description: book.description ? book.description : '',
        isNewBook,
        loadPage: false
      })
    } else {      
      this.setState({
        id: bookSelected.id,
        name: bookSelected.name,
        author: bookSelected.author ? bookSelected.author : '',
        publisher: bookSelected.publisher ? bookSelected.publisher : '',
        quantity: bookSelected.quantity,
        price: bookSelected.unitPrice ? bookSelected.unitPrice : '',
        imageURL: bookSelected.imageURL ? bookSelected.imageURL : '' ,        
        currentImage: bookSelected.imageURL ? bookSelected.imageURL : '' ,        
        description: bookSelected.description ? bookSelected.description : '',
        loadPage: false
      })
    }
  }

  onTitleChange = (event) => {
    const { value } = event.target;
    this.setState({ name: value });
  }

  onAuthorNameChange = (event) => {    
    const { value } = event.target;
    this.setState({ author: value });
  }

  onPriceFormat = (event) => {
    let valor = event.target.value;     
    
    valor = valor + '';
    valor = parseInt(valor.replace(/[\D]+/g,''));
    valor = valor + '';
    valor = valor.replace(/([0-9]{2})$/g, ",$1");

    if (valor.length > 6) {
      valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    }    
    if (valor === '') {
      this.setState({ price: 0  });
    } else {
      this.setState({ price: valor  });
    }    
  }

  onPublisherChange = (event) => {
    const { value } = event.target;
    this.setState({ publisher: value });
  }

  onQuantityChange = (event) => {
    const { value } = event.target;
    this.setState({ quantity: value });
  }

  onChangeDescription = (event) => {
    const { value } = event.target;
    this.setState({ description: value });
  }


  onChangeImageURL = (event) => {    
    const { value } = event.target;
    console.log('nova imagem = ', value);
    this.setState({ imageURL: value, newImage: true });
  }
    
  onChangeImage = e => {        
    const files = Array.from(e.target.files)
    console.log('arquivos = ', files)
    console.log('nome do arquivo ', files[0].name)
    this.setState({ 
      image: URL.createObjectURL(files[0]),
      imageURL: URL.createObjectURL(files[0]) ,
      newImage: true            
    });
  }

  _crop() {    
    try {                  
      this.setState({croppedImage: this.refs.cropper.getCroppedCanvas().toDataURL() });            
    } catch (error) {
      // alert('Não foi possível fazer recorte da imagem');
      // this.setState({ imageURL: '' })
      console.log('Não foi possível fazer recorte da imagem: ', error);            
    }    
  }

  imageToBlog = (dataURI) => {    
      // convert base64 to raw binary data held in a string
      // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
      var byteString = atob(dataURI.split(',')[1]);
  
      // separate out the mime component
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
  
      // write the bytes of the string to an ArrayBuffer
      var ab = new ArrayBuffer(byteString.length);
      var ia = new Uint8Array(ab);
      for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
      }
  
      // write the ArrayBuffer to a blob, and you're done
      var bb = new Blob([ab]);
      return bb;  
  }

  uploadFile = async () => {
    const { croppedImage, name } = this.state;    
    try {
      const extension = '.' + croppedImage.split('/')[1].slice(0,3);            
      const bookName = (+new Date()) + '-' + name;    
      const metadata = { contentType: extension };
      const imageBlob = this.imageToBlog(croppedImage);      
      const storage = firebase.storage().ref('imagensLivros/');
      console.log(croppedImage);  

      const res = await storage.child(bookName + extension).put(imageBlob, metadata);
      console.log('terminou de upar livro');
      this.setState({ loading: false, completeTask: 'completed' });      
      // return await res.ref.getDownloadURL();                     
    } catch (error) {
      this.setState({ loading: false, completeTask: 'failed' });      
    }    
  }

  uploadBook = async () => {
    const { 
      id,
      name,
      author,
      publisher,
      price,      
      quantity,
      imageURL,      
      newImage,
      description,
      isNewBook
    } = this.state;   
    
    this.setState({ loading: true })
    ReactDOM.findDOMNode(this).scrollTo({ y: 0}); 

    let newBook;

    if (newImage) {      
      const url = await this.uploadFile();      
      newBook = {        
        name,
        author,
        publisher,
        quantity,
        unitPrice: price.replace(',', '.'),
        imageURL: url,
        description
      }      
    } else {
      newBook = {        
        name,
        author,
        publisher,
        quantity,
        unitPrice: price.replace(',', '.'),
        imageURL,
        description
      }
    }  
    
    try {
      if (isNewBook) {        
        await this.props.createBook(newBook);
      } else {        
        await this.props.updateBoook(newBook, id);
      }            
      ReactDOM.findDOMNode(this).scrollIntoView();
      this.props.history.push('/');
    } catch (error) {
      console.log(error);
      this.setState({ loading: false, completeTask: 'failed' })            
    }      
  }

  onRenderMessage = () => {
    const { completeTask } = this.state;
    switch(completeTask) {
      case 'completed':
      setTimeout(() => {
        this.setState({ completeTask: false });
      }, 5000)
        return (
        <div className="alert alert-success hidden" role="alert">
          Livro adicionado a livraria!
        </div>)
      case 'failed':        
      setTimeout(() => {
        this.setState({ completeTask: false });
      }, 5000)
        return (
          <div className="alert alert-danger" role="alert">
            Não foi possível adicionar um livro, verifique as informações e tente novamente.
          </div>
        )
      default: 
      return;
    }    
  }



  render() {    
    const { 
      isLogged,      
      name, 
      author,      
      imageURL,      
      publisher,
      quantity,
      currentImage,      
      price,
      description,
      loading,      
      id,
      isNewBook,
      loadPage
    } = this.state;
    let page;
    if (isLogged) {
      page = (
        <div>
        {
          this.onRenderMessage()
        }
          <h1 style={{ color: '#B12704'}}>
            {
              isNewBook ?               
              'Novo Livro'
              :              
              'Editando Livro'
            }
          </h1>
          <form>
            <div className="form-group">
              <label>Título</label>
              <input 
                type="text" 
                className="form-control" 
                id="formGroupExampleInput" 
                placeholder="Título do Livro" 
                onChange={event => this.onTitleChange(event)}
                value={name}
              />
            </div>
            <div className="form-group">
              <label>Autor</label>
              <input 
                type="text" 
                className="form-control" 
                id="formGroupExampleInput2" 
                placeholder="Autor do Livro" 
                onChange={event => this.onAuthorNameChange(event)}
                value={author}
              />
            </div>
          <div className="form-group">
            <label >Preço R$</label>
            <input 
              value={price.replace('.', ',')}
              type="text" 
              className="form-control" 
              name="valor" 
              placeholder="Digite aqui"               
              onChange={event => this.onPriceFormat(event)}
              />  
          </div>
          <div className="form-group">
            <label>Editora</label>
            <input 
              type="text" 
              className="form-control" 
              id="formGroupExampleInput2" 
              placeholder="Autor do Livro" 
              onChange={event => this.onPublisherChange(event)}
              value={publisher}              
            />
          </div>
          <div className="form-group">
            <label>Quantidade em Unidades</label>
            <input 
              type="text" 
              className="form-control" 
              id="formGroupExampleInput2" 
              placeholder="Quantidade Disponível" 
              onChange={event => this.onQuantityChange(event)}
              value={quantity}              
            />
          </div>
          {/* <div className="form-group">            
            <label>Imagem da Web</label>
            <input             
              type="text"  
              className="form-control" 
              id="formGroupExampleInput2" 
              placeholder="URL de uma Imagem" 
              onChange={this.onChangeImageURL}
              value={imageURL}
            />   
          </div>       */}
          <div className="form-group">
            <input 
              type='file' 
              id='multi' 
              onChange={this.onChangeImage}              
              multiple 
              placeholder={'Escolha uma imagem para o Livro'}          
              onClick={() => this.setState({ newImage: true })}
              className="form-control-file" 
            />             
              <Cropper
                ref='cropper'
                src={imageURL}
                style={{height: 300, width: '50%'}}                                                                          
                responsive={true}                
                movable={true}                                           
                crop={this._crop.bind(this)} 
              />             
          </div>
          {
            !isNewBook ?
              <div className="form-group">
                <label>Imagem Atual</label>                            
                <div>
                <img                   
                  src={currentImage}    
                  alt={'Imagem Atual'}            
                /> 
                </div>              
            </div>              
            :
            null
          }          
          <div className="form-group">
            <label>Sinopse ou Descrição</label>
            <textarea 
              className="form-control" 
              id="exampleFormControlTextarea1" 
              rows="6" 
              value={description}
              onChange={event => this.onChangeDescription(event)}
            />
          </div>          
          {
            isNewBook ?
            <button 
              type="button" 
              className="btn btn-success"
              style={{ marginBottom: '40px'}}
              onClick={this.uploadBook}
            >
              Criar Novo Livro
            </button>
            :
            <button 
              type="button" 
              className="btn btn-success"
              style={{ marginBottom: '40px'}}
              onClick={this.uploadBook}
            >            
            Salvar              
            </button>
          }                    
        </form>                   
        {
          loading ? 
          
          <CircularProgress  
                       
            style={{ 
              color: 'rgb(17, 117, 25)',
              marginTop: '20px'
            }}                    
          />             
          :
          'Salvar'
        }    
        </div>
      );
    } 
    else  {
      const book = {
        id,
        name,
        author,
        quantity,
        publisher,
        price,
        imageURL,
        currentImage,
        description
      };
      page = (
        <BookDescription         
          book={book}
        />
      )
    } 


    return (
      <div>
        <div 
          className='container' 
          style={{ 
            marginTop: '2em',
            maxWidth: isLogged ? '1140px' : '100%'
            }}>                    
          {loadPage ?          
          <div
            style={{
              width: '100%',              
              display: 'flex',                            
            }}
          >           
              <CircularProgress             
                style={{ 
                  color: 'rgb(17, 117, 25)',
                  margin: '10px auto'
                }}                    
              />           
          </div>            
            :
            page
          }          
        </div>              
      </div>      
        
    )
  }
}


const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,    
  },
});


const mapStateToProps = (state) => ({
  bookSelected: state.bookData.bookSelected  
})

const mapDispatchToProps = {
  getBookById,
  updateBoook,
  createBook  
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BookPage)));
