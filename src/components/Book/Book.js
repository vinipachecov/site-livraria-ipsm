import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { BRL } from '../../helpers';
import { red } from '../../utils/colors';

const Book = (props) => {  
  const {
    data,    
   } = props;   
   const {       
    name } = data;     

  return (    
    <div style={{width: '18rem' }}>  
      <div style={{ flexDirection: 'row', display: 'flex' }}>      
      {
        data.imageURL ?
        <img 
          className="card-img-top" 
          src={data.imageURL} 
          alt={data.name}
          style={{ height: '50%', width: '50%', alignSelf: 'center' }}        
        />      
        :
        <p style={{ alignSelf: 'center', width: 'fit-content'}}>
          Sem Imagem          
        </p>        
      }        
      <div className="card-body">        
        <p className="card-text" style={{ fontSize: '1.2em', fontWeight: 'bold' }}>Título:                       
        </p>
        <p style={{ fontWeight: 'normal'}}>
            <Link     
              onClick={() => props.onPress(data)}
              to={`/book/${data.id}`}
            >
            {name}
            </Link>   
            </p>                      
        <p style={{ fontWeight: 'bold', marginBottom: '0px', }}>Preço:</p>        
        <p style={{ color: red }}>{BRL(parseFloat(data.unitPrice)).format('$1.0')}</p>
        <p>Unidades: {data.quantity}</p>        
      </div>

      </div>         
  </div>  
  );
};

Book.propTypes = {
  data: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired
}

export default Book;
