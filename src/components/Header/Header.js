import React, { Component } from 'react'
import classes from './Header.css';
import { Link, withRouter } from 'react-router-dom';
import Aux from '../../hoc/Aux/Aux';
import firebase from 'firebase'

class Header extends Component {

  signOut = async () => {    
    console.log(this.props);    
    await firebase.auth().signOut();    
    await this.props.onLogOut()
    this.props.history.push('/login');
  }    

  render() {
    const { user } = this.props;    
    return (
       <Aux>
      <header className={["mdl-layout__header", classes.header].join(' ')} >
        <div className={"mdl-layout__header-row"}>    
          <span className="mdl-layout-title">
            <Link
              className={"mdl-navigation__link"}
              to={'/'}
              style={{ textDecoration: 'none', fontWeight: '400'}}          
            >
              Livraria Igreja Presbiteriana
            </Link>        
          </span>
          <div className="mdl-layout-spacer"></div>
          <nav className="mdl-navigation mdl-layout--large-screen-only">                      
          
          <div className="mdl-navigation__link" style={{ fontWeight: 'bold' }}><a href='https://www.youtube.com/channel/UCF2WilNw1-LkA_jYVsY1PyQ'><i className={'fab fa-youtube fa-2x'}></i></a></div>                        {/* https://www.youtube.com/channel/UCF2WilNw1-LkA_jYVsY1PyQ */}
          <div className="mdl-navigation__link" style={{ fontWeight: 'bold' }}><a href='https://www.facebook.com/igrejapresbiterianasantamaria/'><i className={'fab fa-facebook fa-2x'}></i></a></div>                        
          {
            !user  ?
            <Link              
              className={"mdl-navigation__link"}
              to={'/login'}   
              style={{ fontWeight: '500'}}         
            >
              Login
            </Link>
            :
            <Aux>
              <p className="mdl-navigation__link" style={{ fontWeight: 'bold' }}>Bem vindo {user.email}</p>              
            <Link                          
              to="/login"   
              onClick={this.signOut}
              style={{ fontWeight: '500'}}         
            >
              logOut
            </Link>

            </Aux>
          }                          
          </nav>          
        </div>
      </header>
       <div className="mdl-layout__drawer mdl-layout--small-screen-only">
        <span className="mdl-layout-title">Links Úteis</span>
        <nav className="mdl-navigation">        
        <Link
              className={"mdl-navigation__link"}
              to={'/'}
              style={{ textDecoration: 'none', fontWeight: '400'}}          
            >
              Lista de livros
            </Link>    
        {
          !user ?
          <Link
          className="mdl-navigation__link"
          to={'/login'}            
        >
          Login
        </Link>   
        :
        <Link
          className="mdl-navigation__link"
          to={'/login'}            
        >
          logout
        </Link>     
        }
        <div className="mdl-navigation__link">Facebook Oficial <a href='https://www.youtube.com/channel/UCF2WilNw1-LkA_jYVsY1PyQ'><i className={'fab fa-youtube fa-2x'}></i></a></div>                        {/* https://www.youtube.com/channel/UCF2WilNw1-LkA_jYVsY1PyQ */}
          <div className="mdl-navigation__link" >Canal da Youtube <a href='https://www.facebook.com/igrejapresbiterianasantamaria/'><i className={'fab fa-facebook fa-2x'}></i></a></div>                        
        </nav>
      </div>   
    </Aux>          
    )
  }
}

export default withRouter(Header);
