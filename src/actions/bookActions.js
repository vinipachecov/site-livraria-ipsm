import firebase from 'firebase';
import { GET_BOOKS, SELECT_BOOK, GET_BOOK, UPDATED_BOOK, SEND_NEW_BOOK, FETCHING_FILTERED_BOOKS, SEARCH_TEXT_CHANGE } from './ActionTypes';
import escapeRegExp from 'escape-string-regexp';


/**
 * Send books to redux
 */
export const getBooks = () => {
  return async dispatch => {    
    await firebase.database().ref('/livros').on('value', snapshot => {
      const bookList = snapshot.val();      
      const parsedBookList = Object.keys(bookList).map(key => {
       return { id: key, ...bookList[key]}
     })
     dispatch({
       type: GET_BOOKS, payload: parsedBookList
     })                
    });    
  };
}

export const getBookById = (bookId) => {
  return async dispatch => {    
    const res = await firebase.database().ref(`/livros/${bookId}`).once('value');             
    const fetchedBook = { id: bookId, ...res.val() };
    dispatch({ type: GET_BOOK, payload: fetchedBook });
    return fetchedBook;
  };
}

export const selectBook = (book) => ({
  type: SELECT_BOOK,
  payload: book   
});

export const updateBoook = (updatedBook, id) => {
  return async dispatch => {       
    console.log(updatedBook);
    await firebase.database().ref(`/livros/`).update({
      [id]: updatedBook
    });
    dispatch({ type: UPDATED_BOOK, payload: updatedBook });    
  };
}

export const createBook = (newBook) => {
  return async dispatch => {
    try {
        // send request to firebase
        const key = await firebase.database().ref('livros/').push({
          ...newBook
        }).key;        
        dispatch({ type: SEND_NEW_BOOK, payload: { ...newBook, id: key  } })
    } catch (error) {
      console.log(error);      
    }
  }
}

export const getFilteredBooks = (bookList, searchText) => {
    return async dispatch => {
      let showingBooks;
      if (searchText) {        
        const match = new RegExp(escapeRegExp(searchText), 'i');
        showingBooks = bookList.filter((book) => match.test(book.name) || match.test(book.publisher) || match.test(book.author));
        console.log('LISTA DE LIVROS = ', showingBooks);                
        dispatch({ type: FETCHING_FILTERED_BOOKS, payload: showingBooks });
        dispatch({type: SEARCH_TEXT_CHANGE,payload: searchText});        
      } else {
        dispatch({ type: FETCHING_FILTERED_BOOKS, payload: bookList });                
      }      

  }
}

export const sendSearchBookText = (value) => ({
  type: SEARCH_TEXT_CHANGE,
  payload: value
})
