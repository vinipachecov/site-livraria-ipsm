export const GET_BOOKS = 'GET_BOOKS';
export const SELECT_BOOK = 'SELECT_BOOK';
export const GET_BOOK = 'GET_BOOK';
export const UPDATED_BOOK = 'UPDATED_BOOK';
export const SEND_NEW_BOOK = 'SEND_NEW_BOOK';
export const FETCHING_FILTERED_BOOKS = 'FETCHING_FILTERED_BOOKS';
export const SEARCH_TEXT_CHANGE = 'SEARCH_TEXT_CHANGE'

//users
export const SEND_USER = 'SEND_USER';