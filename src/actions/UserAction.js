import { SEND_USER } from "./ActionTypes";
import firebase from 'firebase';

export const sendUser = (user) => ({
  type: SEND_USER,
  payload: user
})

export const checkUserSignedIn = () => {
  return dispatch => {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {                   
        dispatch({ type: SEND_USER, payload: user});
      } else {        
        firebase.auth().signOut();
        dispatch({ type: SEND_USER, payload: ''});
      }             
    }); 
  }
}