import { 
  GET_BOOKS, 
  SELECT_BOOK,
  UPDATED_BOOK,
  SEND_NEW_BOOK,
  FETCHING_FILTERED_BOOKS,
  SEARCH_TEXT_CHANGE
} from "../actions/ActionTypes";

const initialState = {
  bookList: [],  
  bookSelected: {},
  filteredBooks: [],
  searchText: ''
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
  case GET_BOOKS:
    return { 
      ...state, 
      bookList: payload,
      filteredBooks: payload 
    }
  case SELECT_BOOK:
    return { ...state, bookSelected: payload }
  case UPDATED_BOOK:
    return { 
      ...state,
      bookList: [...state.bookList].map(book => {
        return book.id === payload.id ? payload : book
      })
    }
  case SEND_NEW_BOOK:
    return {
      ...state,
      bookList: [...state.bookList, payload],
      filteredBooks: [...state.bookList, payload],

    }
  case FETCHING_FILTERED_BOOKS:
    return {
      ...state,
      filteredBooks: payload
    }
  case SEARCH_TEXT_CHANGE:
    return {
      ...state,
      searchText: payload
    }
  default:
    return state
  }
}
