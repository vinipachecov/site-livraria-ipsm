import { SEND_USER } from "../actions/ActionTypes";

const initialState = {
  user: null
}

export default (state = initialState, action) => {
  switch (action.type) {

  case SEND_USER:
    return { ...state, user: action.payload };

  default:
    return state
  }
};
